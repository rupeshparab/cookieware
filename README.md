# README #

## CookieWare ##

This repository is my solution to the following problem statement:


```
Authentication

Create a simple web application that allows a user to do the following things:
Register with a username and password.
Log in and log out
When the user is logged in, the homepage should show the username. 
Write a custom middleware that sets a cookie on the client whenever a request comes from an authenticated user. The cookie should be named last_access_t, and the value should be the current timestamp in UTC timezone.
```


## Note ##

Th project has been hosted on [Heroku at https://cookieware.herokuapp.com/](https://cookieware.herokuapp.com/)

## Features ##
1. Basic Auth and Registration using username and password
2. Displays username after login
3. Upload file and get a sharable link
4. Throttles max file shares by remote ip of users
5. Rejects files greater than 2MB

## Credits ##
1. [Rails 5](https://github.com/rails/rails)
2. [Bootstrap](https://github.com/twbs/bootstrap)